################################################
## As we can't create IAM role in AWS Learner ##
##      Lab, I disabled this part of code     ##
################################################


# resource "aws_iam_instance_profile" "epita" {
#   name = "epita-instance-profile"

#   role = aws_iam_role.role.name

# }

# resource "aws_iam_role" "role" {
#   name = "epita"
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Principal = {
#           Service = "ec2.amazonaws.com"
#         }
#       }
#     ]
#   })

#   tags = merge(var.tags,
#     {
#       Name        = "iam-epita"
#       Terraform   = "true"
#       Environment = "test"
#   })
# }

# resource "aws_iam_role_policy_attachment" "policy" {
#   role       = aws_iam_role.role.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2RoleforSSM"
# }