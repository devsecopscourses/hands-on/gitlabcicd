#------------------------------------------------------------------------------------ 
# Terraform CHANGELOG : https://github.com/hashicorp/terraform/blob/main/CHANGELOG.md
# Provider AWS CHANGELOG : https://github.com/hashicorp/terraform-provider-aws/blob/main/CHANGELOG.md
#------------------------------------------------------------------------------------
terraform {
  required_providers {
    aws = "5.61.0"
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "random" {
}

#
# Initialisation du backend distant
# https://developer.hashicorp.com/terraform/language/settings/backends/s3
#
terraform {
  backend "s3" {
    region         = "us-east-1"
    bucket         = "mbounaceur-terraform"
    key            = "epita/bounac_m/app.tfstate"
    dynamodb_table = "TerraformLock"
  }
}