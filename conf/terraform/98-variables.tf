variable "region" {
  description = "Region used for deploying on AWS."
  type        = string
  default     = "us-east-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "iam_instance_profile" {
  default = "LabInstanceProfile"
}

variable "tags" {
  description = "Tags mappings"
  type        = map(any)
  default = {
    SCHOOL = "EPITA",
    PROMO  = "APPING X 2021-2024"
    GROUP  = "1"
  }
}

variable "ecr-front" {
  description = "ECR Repository for the frontend"
  type = string
  default = "ecr-front"
}

variable "ecr-back" {
  description = "ECR Repository for the backend"
  type = string
  default = "ecr-back"
}

variable "COMMIT" {
  type = string
}

variable "BRANCH" {
  type = string
}

variable "USER" {
  type = string
}